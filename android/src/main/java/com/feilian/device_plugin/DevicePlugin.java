package com.feilian.device_plugin;

import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import com.sinocare.multicriteriasdk.MulticriteriaSDKManager;
import com.sinocare.multicriteriasdk.SnCallBack;
import com.sinocare.multicriteriasdk.auth.AuthStatusListener;
import com.sinocare.multicriteriasdk.bean.BaseDetectionData;
import com.sinocare.multicriteriasdk.entity.BoothDeviceConnectState;
import com.sinocare.multicriteriasdk.entity.DeviceDetectionState;
import com.sinocare.multicriteriasdk.entity.SNDevice;
import com.sinocare.multicriteriasdk.utils.AuthStatus;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * DevicePlugin
 */
public class DevicePlugin implements FlutterPlugin, ActivityAware, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native
    /// Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine
    /// and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;
    Application application;
    Activity activity;
    Boolean isAuthed = false;
    Boolean isAddListener = false;
    private ArrayList<SNDevice> snDevices = new ArrayList<>();

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "device_plugin");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + Build.VERSION.RELEASE);
        } else if (call.method.equals("init")) {
            MulticriteriaSDKManager.init(application);
            result.success("MulticriteriaSDKManager  success");
        } else if (call.method.equals("auth")) {
            if (!isAuthed) {
                MulticriteriaSDKManager.authentication(new AuthStatusListener() {
                    @Override
                    public void onAuthStatus(AuthStatus authStatus) {
                        isAuthed = true;
                        result.success("MulticriteriaSDKManager" + authStatus.toString());
                    }
                });
            } else {
                result.success("MulticriteriaSDKManager");
            }

        } else if (call.method.equals("startMeasuring")) {
            SNDevice snDevice = new SNDevice();
            Map arguments = (Map) call.arguments;
            snDevice.setProductCode((String) arguments.get("productCode"));
            snDevice.setDataProtocolCode(SNDevice.DEVICE_ONE_TEST_BPG_BLE);
            snDevice.setMachineCode((String) arguments.get("machineCode"));
            snDevice.setMac((String) arguments.get("mac"));
            MulticriteriaSDKManager.startMeasuring(snDevice);
            result.success("开始测量");
        } else if (call.method.equals("disConnectDevice")) {
            Map arguments = (Map) call.arguments;
            SNDevice snDevice = new SNDevice();
            snDevice.setName((String) arguments.get("name"));
            snDevice.setProductCode((String) arguments.get("productCode"));
            snDevice.setDataProtocolCode(SNDevice.DEVICE_ONE_TEST_BPG_BLE);
            if (Objects.requireNonNull(arguments.get("protocolCode")).equals("ug_11_ble")) {
                snDevice.setDataProtocolCode(SNDevice.DEVICE_UG_11_BLE);
            }
            snDevice.setMachineCode((String) arguments.get("machineCode"));
            snDevice.setMac((String) arguments.get("mac"));
            snDevices.add(snDevice);
            MulticriteriaSDKManager.disConectDevice(snDevices);
            result.success("");
        } else if (call.method.equals("connectDevice")) {
            // TODO 连接指定设备
            Map<?, ?> arguments = (Map) call.arguments;
            SNDevice snDevice = new SNDevice();
            snDevice.setName((String) arguments.get("name"));
            snDevice.setProductCode((String) arguments.get("productCode"));
            snDevice.setDataProtocolCode(SNDevice.DEVICE_ONE_TEST_BPG_BLE);
            switch ((String) Objects.requireNonNull(arguments.get("protocolCode"))) {
                case "ug_11_ble":
                    snDevice.setDataProtocolCode(SNDevice.DEVICE_UG_11_BLE);
                    break;
                case "slx_120_ble":
                    snDevice.setDataProtocolCode(SNDevice.DEVICE_SLX_120_BLE);
                    break;
                case "safe_aq_air_ble":
                    snDevice.setDataProtocolCode(SNDevice.DEVICE_AQ_AIR_BLE);
                    break;
            }
            snDevice.setMachineCode((String) arguments.get("machineCode"));
            snDevice.setMac((String) arguments.get("mac"));
            snDevices.add(snDevice);
            result.success("开始连接");
            if (!isAddListener) {
                isAddListener = true;
                MulticriteriaSDKManager.startConnect(snDevices, getSnCallBack());
            } else {
                MulticriteriaSDKManager.startConnect(snDevices);
            }
        } else {
            result.notImplemented();
        }
    }

    @NonNull
    private SnCallBack getSnCallBack() {
        return new SnCallBack() {
            @Override
            public void onDataComing(SNDevice device, BaseDetectionData data) {
                // 设备数据回调，解析见后面数据结构，实时测量数据与历史测量数据均在此处回调；
                Log.e("onDataComing", data.getData());
                HashMap<String, String> map = new HashMap<>();
                map.put("name", device.getName());
                map.put("mac", device.getMac());
                map.put("data", data.getData());
                channel.invokeMethod("onDataComing", map);
            }

            @Override
            public void onDetectionStateChange(SNDevice device, DeviceDetectionState state) {
                // 设备数据状态：时间同步成功、历史数据获取成功、清除成功等等
            }

            @Override
            public void onDeviceStateChange(SNDevice device, BoothDeviceConnectState state) {
                // 连接连接状态 目前只回调连接成功与断开连接
                Log.e("onDeviceStateChange", state.getDesc());
                HashMap<String, String> map = new HashMap<>();
                map.put("name", device.getName());
                map.put("mac", device.getMac());
                map.put("mState", String.valueOf(state.getmState()));
                map.put("bleNamePrefix", device.getBleNamePrefix());
                channel.invokeMethod("onDeviceStateChange", map);
            }
        };
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding activityPluginBinding) {
        application = activityPluginBinding.getActivity().getApplication();
        activity = activityPluginBinding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding activityPluginBinding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }

    private boolean isAndroid12() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.S;
    }
}
