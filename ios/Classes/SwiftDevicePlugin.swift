import Flutter
import UIKit
import SinoDetection

public class SwiftDevicePlugin: NSObject, FlutterPlugin {
    static var channel: FlutterMethodChannel?
    static let blManager = SDBluetoothManager.shared()

    public static func register(with registrar: FlutterPluginRegistrar) {
        print("Init device plugin")
        let channel = FlutterMethodChannel(name: "device_plugin", binaryMessenger: registrar.messenger())
        let instance = SwiftDevicePlugin()
        SwiftDevicePlugin.channel = channel;
        channel.setMethodCallHandler { (call, result) in
            instance.handle(call, result: result)
        }
        registrar.addMethodCallDelegate(instance, channel: channel)
    }


    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        print("Method channel device_plugin call: \(call.method), args: \(String(describing: call.arguments))")
        switch (call.method) {
        case "init":
            result("iOS " + UIDevice.current.systemVersion)
            break
        case "auth":
            auth(call, result: result)
            break
        case "connectDevice":
            onDataComingOrStateChange()
            connectDevice(call, result: result)
            break
        case "disConnectDevice":
            disConnectDevice(call, result: result)
            break
        default:
            print("三诺插件 无效的方法, method: \(call.method)")
            result("nil")
        }
    }

    public func auth(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let key: String = call.arguments as! String
        print("三诺开始认证，appKey： \(key)")
        SDAuthManager.shared().auth(withAppKey: key, bundleId: Bundle.main.bundleIdentifier!)
        result("success")
    }

    public func connectDevice(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arg = call.arguments as! Dictionary<String, String>
        let device = getSDDeviceModel(arg: arg)
        var devices = SwiftDevicePlugin.blManager.boundDevices;
        if (devices.isEmpty) {
            devices = [device]
        } else if (!devices.contains(where: { (e) in e.mac == device.mac })) {
            devices.append(device)
        }
        let log = devices.map({ d in
                    "mac:\(d.mac),name:\(d.deviceName)"
                })
                .joined(separator: ";")
        print("开始连接设备:\(log)")
        SwiftDevicePlugin.blManager.boundDevices = devices
        SwiftDevicePlugin.blManager.connectDevices()
        result("开始连接设备")
    }

    ///
    ///        device.machineCode = "0012"
    ///        device.deviceName = arg["name"] ?? ""
    ///        device.deviceType = SDCDeviceType.DEVICE_AQ_AIR_BLE
    ///        device.bluetoothPrefixName = "AW+ AIR-R02305"
    ///        device.bluetoothType = "ble"
    ///        device.productCode = "100004"
    ///        device.mac = "684E05E6F99F"
    ///        device.dataProtocolCode = "safe_aq_air_ble"

    public func startMeasuring(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arg = call.arguments as! Dictionary<String, String>
        let device = getSDDeviceModel(arg: arg)
        let cmd = SDCCommandType(rawValue: UInt(arg["cmd"] ?? "0")!)
        SwiftDevicePlugin.blManager.write(cmd!, content: nil, boundDevice: device)
        result("开始测量")
    }


    public func onDataComingOrStateChange() {
        SwiftDevicePlugin.blManager.didReceiveData = { (dic: [AnyHashable: Any]?, state: SDBussinessStateModel?, device: SDDeviceModel) in
            var data = [String: Any]()
            data["name"] = device.deviceName
            data["mac"] = device.mac
            print("测量数据：\(dic ?? ["": ""]), 状态：\(state?.state.rawValue ?? 100), 设备蓝牙名称：\(device.deviceName), 蓝牙mac：\(device.mac)")
            if (dic?["data"] != nil) {
                let d: NSData! = try? JSONSerialization.data(withJSONObject: dic?["data"]! as Any, options: []) as NSData?
                let JSONString = NSString(data: d as Data, encoding: String.Encoding.utf8.rawValue)
                data["data"] = JSONString! as String
                data["mState"] = "31"
            }
            if (dic != nil
                    && (dic?["data"] is [AnyHashable: Any])
                    && (((dic?["data"] as! [AnyHashable: Any])["type"] != nil) || (dic?["code"] == nil || dic?["code"] as! String != "B7"))
               ) {
                print("回调测量结果")
                SwiftDevicePlugin.channel?.invokeMethod("onDataComing", arguments: data)
            }
            if (state != nil || data["mState"] != nil) {
                if data["mState"] == nil {
                    data["mState"] = "\(state!.state.rawValue)"
                }
                print("回调设备状态: \(data["mState"] ?? "无")")
                SwiftDevicePlugin.channel?.invokeMethod("onDeviceStateChange", arguments: data)
            }
        }
        SwiftDevicePlugin.blManager.didUpdateUUIDString = { (id: String, device: SDDeviceModel) in
            print("更新设备id: \(id), 设备： \(device)")
        }
    }

    public func disConnectDevice(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
//        let arg = call.arguments as! Dictionary<String, String>
//        let tmp = getSDDeviceModel(arg: arg)
        let allDevice = SwiftDevicePlugin.blManager.boundDevices.map({ d in
                    "mac:\(d.mac),name:\(d.deviceName)"
                })
                .joined(separator: ";")
        print("所有连接设备: \(allDevice)")
        if SwiftDevicePlugin.blManager.boundDevices.isEmpty {
            return
        }
//        let device = SwiftDevicePlugin.blManager.boundDevices.filter { (element: SDDeviceModel) in
//                    element.mac == tmp.mac
//                }
//                .first
        SwiftDevicePlugin.blManager.disconnectDevices();
        if (!SwiftDevicePlugin.blManager.boundDevices.isEmpty) {
            SwiftDevicePlugin.blManager.boundDevices.removeAll()
        }
//        if (device != nil) {
//            print("断开设备连接，mac：\(device!.mac), name:\(device!.deviceName)")
//            SwiftDevicePlugin.blManager.disconnectDevice(withBoundDevice: device!);
//            if (!SwiftDevicePlugin.blManager.boundDevices.isEmpty) {
//                SwiftDevicePlugin.blManager.boundDevices.removeAll(where: { d in d.mac == device?.mac })
//                let log = SwiftDevicePlugin.blManager.boundDevices.map({ d in
//                            "mac:\(d.mac),name:\(d.deviceName)"
//                        })
//                        .joined(separator: ";")
//                print("剩余连接设备：\(log)")
//            }
//        }
    }

    func getSDDeviceModel(arg: [String: String]) -> SDDeviceModel {
        let device = SDDeviceModel()
        device.mac = arg["mac"] ?? ""
        device.productCode = arg["productCode"] ?? ""
        device.deviceName = arg["name"] ?? ""
        let type = UInt(arg["sinoType"] ?? "0");
        device.deviceType = SDCDeviceType.init(rawValue: type!)!
        device.dataProtocolCode = arg["protocolCode"] ?? ""
        if (arg["machineCode"] != nil) {
            device.machineCode = arg["machineCode"]!
        }
        return device
    }

}
