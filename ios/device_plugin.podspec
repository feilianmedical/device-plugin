#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint device_plugin.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'device_plugin'
  s.version          = '0.0.1'
  s.summary          = 'device_plugin'
  s.description      = <<-DESC
device_plugin
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.'}
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'SinoSDKFramework'
  s.platform = :ios, '10.0'
  s.static_framework = true

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'
end
