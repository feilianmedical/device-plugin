import 'dart:async';

import 'package:device_plugin/device_plugin.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  final _devicePlugin = DevicePlugin();

  // late Map mac;
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // _devicePlugin.addListener((call)=> {
    //   debugger()
    // });
    try {
      platformVersion =
          await _devicePlugin.init() ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: [
            Text('Running on: $_platformVersion\n'),
            ButtonBar(
              children: [
                TextButton(
                  onPressed: () => _devicePlugin.init(),
                  child: Text("初始化"),
                ),
                TextButton(
                  onPressed: () {
                    _devicePlugin.auth(
                        iosAppkey: 'b9d6ecfdee71a652189cb1a48a0df2ac');
                  },
                  child: Text("鉴权"),
                ),
                StreamBuilder<List<ScanResult>>(
                    stream: FlutterBluePlus.scanResults,
                    initialData: const [],
                    builder: (c, snapshot) {
                      for (var item in snapshot.data!) {
                        Map mac = {
                          "mac": item.device.id.toString(),
                          "productCode": "100006",
                          "protocolCode": "ug_11_ble",
                          "machineCode": "0020"
                        };
                        if (item.device.name == "ClinkBlood") {
                          mac = {
                            "mac": item.device.id.toString(),
                            "productCode": "100019",
                            "protocolCode": "one_test_BPG_ble",
                            "machineCode": ""
                          };
                          debounce(() => connectDevice(mac), 3000)();
                        }

                        if (item.device.name == "BDE_WEIXIN_TTM") {
                          mac = {
                            "mac": item.device.id.toString(),
                            "productCode": "100006",
                            "protocolCode": "ug_11_ble",
                            "machineCode": "0020"
                          };
                          debounce(() => connectDevice(mac), 3000)();
                        }
                      }

                      return TextButton(
                        onPressed: () async {
                          FlutterBluePlus.startScan(
                              timeout: const Duration(seconds: 4));
                        },
                        child: StreamBuilder<bool>(
                          stream: FlutterBluePlus.isScanning,
                          initialData: false,
                          builder: (c, snapshot) {
                            if (snapshot.data!) {
                              return const Text("扫描中");
                            } else {
                              return const Text("扫描蓝牙设备");
                            }
                          },
                        ),
                      );
                    }),
                TextButton(
                  onPressed: () async {
                    List<BluetoothDevice> bluetoothdevices =
                        await FlutterBluePlus.connectedDevices;
                    // print("绑定设备为：" + bluetoothdevices.toString());
                    // bluetoothdevices.forEach((item) {
                    //   print("绑定设备为：" + item.name);
                    // });
                  },
                  child: Text("获取绑定设备列表"),
                ),
                TextButton(
                  onPressed: () {
                    _devicePlugin.onDataComing((obj) {
                      print("===============onDataComing==================");
                      print(obj.toString());
                      print("===============onDataComing==================");
                    });
                  },
                  child: Text("监听测量数据"),
                ),
                TextButton(
                  onPressed: () {
                    _devicePlugin.onDeviceStateChange((obj) {
                      print("===============onDataComing==================");
                      print(obj.toString());
                      print("===============onDataComing==================");
                    });
                  },
                  child: Text("监听设备状态变化"),
                ),
                TextButton(
                  onPressed: () async {
                    List<BluetoothDevice> bluetoothdevices =
                        await FlutterBluePlus.connectedDevices;
                    bluetoothdevices.forEach((element) {
                      if (element.name == 'ClinkBlood') {
                        Map map = {
                          "mac": element.id.toString(),
                          "productCode": "100019",
                          "protocolCode": "one_test_BPG_ble",
                          "machineCode": ""
                        };
                        _devicePlugin.startMeasuring(map);
                      }
                      ;
                    });
                  },
                  child: Text("开始测量（只针对血压计）"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  final Map<String, Timer> _funcDebounce = {};

  void connectDevice(mac) {
    _devicePlugin.connectDevice(mac);
  }

  Function debounce(Function func, [int milliseconds = 2000]) {
    Function target = () {
      String key = func.hashCode.toString();
      Timer? _timer = _funcDebounce[key];
      if (_timer == null) {
        func?.call();
        _timer = Timer(Duration(milliseconds: milliseconds), () {
          Timer? t = _funcDebounce.remove(key);
          t?.cancel();
          t = null;
        });
        _funcDebounce[key] = _timer;
      }
    };
    return target;
  }
}
