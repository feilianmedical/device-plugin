import 'package:device_plugin/device_plugin.dart';
import 'package:device_plugin/device_plugin_method_channel.dart';
import 'package:device_plugin/device_plugin_platform_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockDevicePluginPlatform
    with MockPlatformInterfaceMixin
    implements DevicePluginPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<String?> init() {
    // TODO: implement init
    throw UnimplementedError();
  }

  @override
  Future<String?> auth({String? iosSinoKey}) {
    // TODO: implement auth
    throw UnimplementedError();
  }

  @override
  Future<String?> scanBles() {
    // TODO: implement scanBles
    throw UnimplementedError();
  }

  @override
  Future<String?> addListener(listtner) {
    // TODO: implement addListener
    throw UnimplementedError();
  }

  @override
  Future<String?> connectDevice(obj) {
    // TODO: implement connectDevice
    throw UnimplementedError();
  }

  @override
  Future onDataComing(callback) {
    // TODO: implement onDataComing
    throw UnimplementedError();
  }

  @override
  Future<String?> startMeasuring(obj) {
    // TODO: implement startMeasuring
    throw UnimplementedError();
  }

  @override
  Future onDeviceStateChange(callback) {
    // TODO: implement onDeviceStateChange
    throw UnimplementedError();
  }
  
  @override
  Future<String?> clearAllListner() {
    // TODO: implement clearAllListner
    throw UnimplementedError();
  }

  @override
  Future<String?> disConnectDevice(obj) {
    // TODO: implement disConnectDevice
    throw UnimplementedError();
  }
}

void main() {
  final DevicePluginPlatform initialPlatform = DevicePluginPlatform.instance;

  test('$MethodChannelDevicePlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelDevicePlugin>());
  });

  test('getPlatformVersion', () async {
    DevicePlugin devicePlugin = DevicePlugin();
    MockDevicePluginPlatform fakePlatform = MockDevicePluginPlatform();
    DevicePluginPlatform.instance = fakePlatform;

    expect(await devicePlugin.getPlatformVersion(), '42');
  });
}
