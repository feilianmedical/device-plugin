import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'device_plugin_method_channel.dart';

abstract class DevicePluginPlatform extends PlatformInterface {
  /// Constructs a DevicePluginPlatform.
  DevicePluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static DevicePluginPlatform _instance = MethodChannelDevicePlugin();

  /// The default instance of [DevicePluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelDevicePlugin].
  static DevicePluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [DevicePluginPlatform] when
  /// they register themselves.
  static set instance(DevicePluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String?> init() {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<String?> auth({String? iosSinoKey}) {
    throw UnimplementedError('auth() has not been implemented.');
  }

  Future<String?> connectDevice(obj) {
    throw UnimplementedError('scanBles() has not been implemented.');
  }

  Future<dynamic> onDataComing(callback) {
    throw UnimplementedError('onDataComing() has not been implemented.');
  }

  Future<dynamic> onDeviceStateChange(callback) {
    throw UnimplementedError('onDeviceStateChange() has not been implemented.');
  }

  Future<String?> startMeasuring(obj) {
    throw UnimplementedError('startMeasuring() has not been implemented.');
  }

  Future<dynamic> clearAllListner() {
    throw UnimplementedError('clearAllListner() has not been implemented.');
  }

  Future<String?> disConnectDevice(obj) {
    throw UnimplementedError('断开某个设备蓝牙连接');
  }
}
