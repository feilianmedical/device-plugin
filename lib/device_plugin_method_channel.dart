import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'device_plugin_platform_interface.dart';

/// An implementation of [DevicePluginPlatform] that uses method channels.
class MethodChannelDevicePlugin extends DevicePluginPlatform {
  /// The method channel used to interact with the native platform.
  final List<Map<String, dynamic>> _methodStreamController = [];

  @visibleForTesting
  final methodChannel = const MethodChannel('device_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<String?> init() async {
    methodChannel.setMethodCallHandler(callHandler);
    final version = await methodChannel.invokeMethod<String>('init');
    print('Version: $version');
    return version;
  }

  Future callHandler(MethodCall call) async {
    _methodStreamController
        .where((element) => element["name"] == call.method)
        .forEach((element) {
      print(call.arguments);
      element["callback"](call.arguments);
    });
  }

  @override
  Future<String?> auth({String? iosSinoKey}) async {
    final result = await methodChannel.invokeMethod<String>("auth", iosSinoKey);
    print('Auth result $result');
    return result;
  }

  @override
  Future<String?> connectDevice(obj) async {
    final version =
        await methodChannel.invokeMethod<String>('connectDevice', obj);
    return version;
  }

  @override
  Future<String?> startMeasuring(obj) async {
    final str = await methodChannel.invokeMethod<String>('startMeasuring', obj);
    return str;
  }

  @override
  Future<dynamic> onDataComing(callback) async {
    _methodStreamController.add({"name": "onDataComing", "callback": callback});
  }

  @override
  Future<dynamic> onDeviceStateChange(callback) async {
    _methodStreamController
        .add({"name": "onDeviceStateChange", "callback": callback});
  }

  @override
  Future<dynamic> clearAllListner() async {
    _methodStreamController.clear();
  }

  @override
  Future<String?> disConnectDevice(obj) async {
    return await methodChannel.invokeMethod<String>('disConnectDevice', obj);
  }
}
