import 'device_plugin_platform_interface.dart';

class DevicePlugin {
  Future<String?> getPlatformVersion() {
    return DevicePluginPlatform.instance.getPlatformVersion();
  }

  Future<String?> init() {
    return DevicePluginPlatform.instance.init();
  }

  Future<String?> auth({String? iosAppkey}) {
    return DevicePluginPlatform.instance.auth(iosSinoKey: iosAppkey);
  }

  Future<String?> connectDevice(mac) {
    return DevicePluginPlatform.instance.connectDevice(mac);
  }

  Future<String?> disConnectDevice(mac) {
    return DevicePluginPlatform.instance.disConnectDevice(mac);
  }

  Future<dynamic> onDataComing(callback) {
    return DevicePluginPlatform.instance.onDataComing(callback);
  }

  Future<dynamic> onDeviceStateChange(callback) {
    return DevicePluginPlatform.instance.onDeviceStateChange(callback);
  }

  Future<String?> startMeasuring(mac) {
    return DevicePluginPlatform.instance.startMeasuring(mac);
  }

  Future<dynamic> clearAllListner() {
    return DevicePluginPlatform.instance.clearAllListner();
  }

  
}
